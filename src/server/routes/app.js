// Invoke 'strict' JavaScript mode
'use strict';

var path = require('path');

// Define the routes module' method
module.exports = function (app) {

    app.get('/*', function (req, res) {
      
        return res.sendFile("main.html", { root: path.join(__dirname, 'www') });
    });
};
