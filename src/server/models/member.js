var mongoose = require('mongoose'),
    mongoosePaginate = require('mongoose-paginate'),
    crypto = require('crypto');

var memberSchema = new mongoose.Schema({
    uid: {
        type: String,
        index: true,
        unique: true
    },
    mail: {
        type: String,
        index: true,
        unique: true        
    },
    pwd: String,
    fn: String,
    ln: String,
    bth: Date,
    //avata
    avt: String,

    //website
    www: String,
    bio: String,

    stt: {
        type: Number,
        default: 0
    },

    // last action
    act: {
      type: Date,
      default: Date.now()
    },
    crt: {
        type: Date,
        default: Date.now()
    }

}, {
        collection: 'members'
    }).plugin(mongoosePaginate);


memberSchema.methods.hashPassword = function (password) {
    //return hash.generate(password);
    return crypto.createHash('sha256').update(password).digest('hex');
};
memberSchema.methods.authenticate = function (password) {
    return this.hashPassword(password) == this.pwd;
};

module.exports = mongoose.model('member', memberSchema);
