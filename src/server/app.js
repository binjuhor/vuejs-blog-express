// Invoke 'strict' JavaScript mode
'use strict';

var cluster = require('cluster');

if (cluster.isMaster) {
    var numWorkers = require('os').cpus().length;

    console.log('Master cluster setting up ' + numWorkers + ' workers...');

    for (var i = 0; i < numWorkers; i++) {
        cluster.fork();
    }

    cluster.on('online', function (worker) {
        console.log('Worker ' + worker.process.pid + ' is online');
    });

    cluster.on('exit', function (worker, code, signal) {
        console.log('Worker ' + worker.process.pid + ' died with code: ' + code + ', and signal: ' + signal);
        console.log('Starting a new worker');
        cluster.fork();
    });
} else {

    // Load the module dependencies
    var express = require('express'),
        path = require('path'),
        bodyParser = require('body-parser'),
        cookieParser = require('cookie-parser'),
        methodOverride = require('method-override'),
        session = require('express-session'),
        passport = require('passport'),
        mongoose = require('mongoose'),
        config = require('./config');

    var SessionStore = require('connect-mongo')(session);

    // Create a new Express application instance
    var app = express();

    // app.use(compress());

    // Use the 'body-parser' and 'method-override' middleware functions
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use(cookieParser());
    app.use(methodOverride());

    //Cross-site request forgery protection for Express Node.js framework
    //   app.use(csrf.check());

    // Create a new Mongoose connection instance
    mongoose.connect(config.db);

    // Configure the 'session' middleware
    app.use(session({
        saveUninitialized: false, // don't create session until something stored
        resave: false, //don't save session if unmodified
        secret: config.secret,
        // cookie: {
        //   httpOnly: true,
        //   secure: true
        // },

        store: new SessionStore({ mongooseConnection: mongoose.connection })

    }));

    app.set('trust proxy', 1) // trust first proxy

    // Configure the Passport middleware
    app.use(passport.initialize());
    app.use(passport.session());

    // Configure static file serving
    //app.use(express.static(path.join(__dirname, 'www')));
    app.use(express.static('www'));


    // Configure the Passport middleware
    //require('./passport')();

    require('./routes/api.js')(app);

    // require('./routes/cpanel.js')(app);

    //require('./routes/member.js')(app);
    //require('./routes/upload.js')(app);
    require('./routes/app.js')(app);

    // Use the Express application instance to listen to the '3000' port
    app.listen(config.port.www, function () {
        console.log('Express application instance to listen to the port: ' + config.port.www);
    });

    // Return the Express application instance
    module.exports = app;

}
