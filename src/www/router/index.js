import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import P404 from '../views/404.vue'
import Detail from '../views/post/detail.vue'
import List from '../views/post/list.vue'

const router = new Router({
  mode: 'history',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    { path: '/p/:id', name:'post-detail', component: Detail },
    { path: '/p/:id/:slug', name:'post-detail-slug', component: Detail },
    { path: '/:slug?', component: List },
    { path: '*', component: P404 }
  ]
})

export default router
